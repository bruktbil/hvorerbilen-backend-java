package domain;

import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Version;

abstract class BaseEntity {
    @Id
    private String id;

    @Version
    private Long version;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }
}
