package domain;

public class Labour {
    private String description;
    private String location;
    private String fromDay;
    private String fromMonth;
    private String fromYear;
    private String fromHour;
    private String fromMinute;
    private String toDay;
    private String toMonth;
    private String toYear;
    private String toHour;
    private String toMinute;
    private boolean finished;

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(final String location) {
        this.location = location;
    }

    public String getFromDay() {
        return fromDay;
    }

    public void setFromDay(final String fromDay) {
        this.fromDay = fromDay;
    }

    public String getFromMonth() {
        return fromMonth;
    }

    public void setFromMonth(final String fromMonth) {
        this.fromMonth = fromMonth;
    }

    public String getFromYear() {
        return fromYear;
    }

    public void setFromYear(final String fromYear) {
        this.fromYear = fromYear;
    }

    public String getFromHour() {
        return fromHour;
    }

    public void setFromHour(final String fromHour) {
        this.fromHour = fromHour;
    }

    public String getFromMinute() {
        return fromMinute;
    }

    public void setFromMinute(final String fromMinute) {
        this.fromMinute = fromMinute;
    }

    public String getToDay() {
        return toDay;
    }

    public void setToDay(final String toDay) {
        this.toDay = toDay;
    }

    public String getToMonth() {
        return toMonth;
    }

    public void setToMonth(final String toMonth) {
        this.toMonth = toMonth;
    }

    public String getToYear() {
        return toYear;
    }

    public void setToYear(final String toYear) {
        this.toYear = toYear;
    }

    public String getToHour() {
        return toHour;
    }

    public void setToHour(final String toHour) {
        this.toHour = toHour;
    }

    public String getToMinute() {
        return toMinute;
    }

    public void setToMinute(final String toMinute) {
        this.toMinute = toMinute;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(final boolean finished) {
        this.finished = finished;
    }
}
