package domain;

import java.util.Objects;

public class Comment {
    private SessionStep category;
    private String text;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Comment comment = (Comment) o;
        return category == comment.category &&
                Objects.equals(text, comment.text);
    }

    @Override
    public int hashCode() {
        return Objects.hash(category, text);
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public SessionStep getCategory() {
        return category;
    }

    public void setCategory(SessionStep category) {
        this.category = category;
    }
}
