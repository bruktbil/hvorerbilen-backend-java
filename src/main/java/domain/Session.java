package domain;

import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Property;

import java.util.ArrayList;
import java.util.List;

public class Session extends BaseEntity {
    public static final String CAR_INFO_FIELD_NAME = "carInfo";
    public static final String USED_CAR_INFO_FIELD_NAME = "usedCarInfo";
    public static final String LABOUR_OPERATIONS_FIELD_NAME = "labourOperations";
    public static final String CURRENT_STEP_FIELD_NAME = "currentStep";
    public static final String COMMENTS_FIELD_NAME = "comments";
    public static final String LOG_ITEMSS_FIELD_NAME = "logItems";
    public static final String SESSION_STATES_FIELD_NAME = "states";
    public static final String FINISHED_IN_PROCESS_FIELD_NAME = "finishedInProcess";
    public static final String SOLD_FIELD_NAME = "sold";
    public static final String ACQUISITION_CAR_FIELD_NAME = "acquisitionCar";

    @Embedded(CAR_INFO_FIELD_NAME)
    private CarInfo carInfo;
    @Embedded(USED_CAR_INFO_FIELD_NAME)
    private UsedCarInfo usedCarInfo;
    @Embedded(LABOUR_OPERATIONS_FIELD_NAME)
    private List<Labour> labourOperations;
    @Embedded(COMMENTS_FIELD_NAME)
    private List<Comment> comments = new ArrayList<>();
    @Embedded(LOG_ITEMSS_FIELD_NAME)
    private List<LogItem> logItems;
    @Embedded(SESSION_STATES_FIELD_NAME)
    private SessionStates states = new SessionStates();
    @Embedded(CURRENT_STEP_FIELD_NAME)
    private SessionStep currentStep = SessionStep.NOT_STARTED;
    @Property(FINISHED_IN_PROCESS_FIELD_NAME)
    private boolean finishedInProcess;
    @Property(SOLD_FIELD_NAME)
    private boolean sold;
    @Property(ACQUISITION_CAR_FIELD_NAME)
    private boolean acquisitionCar;

    public boolean isFinishedInProcess() {
        return finishedInProcess;
    }

    public void setFinishedInProcess(boolean finishedInProcess) {
        this.finishedInProcess = finishedInProcess;
    }

    public boolean isSold() {
        return sold;
    }

    public void setSold(boolean sold) {
        this.sold = sold;
    }

    public boolean isAcquisitionCar() {
        return acquisitionCar;
    }

    public void setAcquisitionCar(boolean acquisitionCar) {
        this.acquisitionCar = acquisitionCar;
    }

    public SessionStates getStates() {
        return states;
    }

    public void setStates(SessionStates states) {
        this.states = states;
    }

    public CarInfo getCarInfo() {
        return carInfo;
    }

    public void setCarInfo(CarInfo carInfo) {
        this.carInfo = carInfo;
    }

    public UsedCarInfo getUsedCarInfo() {
        return usedCarInfo;
    }

    public void setUsedCarInfo(UsedCarInfo usedCarInfo) {
        this.usedCarInfo = usedCarInfo;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public List<LogItem> getLogItems() {
        return logItems;
    }

    public void setLogItems(List<LogItem> logItems) {
        this.logItems = logItems;
    }

    public List<Labour> getLabourOperations() {
        return labourOperations;
    }

    public void setLabourOperations(final List<Labour> labourOperations) {
        this.labourOperations = labourOperations;
    }

    public SessionStep getCurrentStep() {
        return currentStep;
    }

    public void setCurrentStep(SessionStep currentStep) {
        this.currentStep = currentStep;
    }
}
