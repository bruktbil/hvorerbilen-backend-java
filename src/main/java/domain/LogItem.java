package domain;

public class LogItem {
    private String type;
    private String date;
    private String userPerformingAction;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUserPerformingAction() {
        return userPerformingAction;
    }

    public void setUserPerformingAction(String userPerformingAction) {
        this.userPerformingAction = userPerformingAction;
    }
}
