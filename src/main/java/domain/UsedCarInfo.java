package domain;

public class UsedCarInfo {
    private String usedCarNumber;
    private String location;
    private String bought;
    private String owner;
    private String assessedByUser;
    private String departmentMtakst;

    public final String getUsedCarNumber() {
        return usedCarNumber;
    }

    public void setUsedCarNumber(final String usedCarNumber) {
        this.usedCarNumber = usedCarNumber;
    }

    public final String getLocation() {
        return location;
    }

    public void setLocation(final String location) {
        this.location = location;
    }

    public final String getBought() {
        return bought;
    }

    public void setBought(final String bought) {
        this.bought = bought;
    }

    public final String getOwner() {
        return owner;
    }

    public void setOwner(final String owner) {
        this.owner = owner;
    }

    public final String getAssessedByUser() {
        return assessedByUser;
    }

    public void setAssessedByUser(final String assessedByUser) {
        this.assessedByUser = assessedByUser;
    }

    public final String getDepartmentMtakst() {
        return departmentMtakst;
    }

    public void setDepartmentMtakst(final String departmentMtakst) {
        this.departmentMtakst = departmentMtakst;
    }
}
