package domain;

public enum SessionStepState {
    NOT_STARTED("NOT_STARTED"),
    IN_PROGRESS("IN_PROGRESS"),
    FINISHED("FINISHED");

    private final String stepState;

    SessionStepState(final String stepState) {
        this.stepState = stepState;
    }

    public static SessionStepState getOppositeState(final SessionStepState state) {
        switch (state) {
            case IN_PROGRESS:
                return SessionStepState.FINISHED;
            case FINISHED:
                return SessionStepState.IN_PROGRESS;
            default:
            case NOT_STARTED:
                return SessionStepState.IN_PROGRESS;
        }
    }

    public String getStepState() {
        return stepState;
    }
}
