package domain;

public class CarInfo {
    private String registrationNumber;
    private String brand;
    private String model;
    private String color;
    private String mileage;
    private String modelYear;

    public final String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(final String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public final String getBrand() {
        return brand;
    }

    public void setBrand(final String brand) {
        this.brand = brand;
    }

    public final String getModel() {
        return model;
    }

    public void setModel(final String model) {
        this.model = model;
    }

    public final String getColor() {
        return color;
    }

    public void setColor(final String color) {
        this.color = color;
    }

    public final String getMileage() {
        return mileage;
    }

    public void setMileage(final String mileage) {
        this.mileage = mileage;
    }

    public final String getModelYear() {
        return modelYear;
    }

    public void setModelYear(final String modelYear) {
        this.modelYear = modelYear;
    }
}
