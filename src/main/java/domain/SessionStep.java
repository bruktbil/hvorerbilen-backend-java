package domain;

public enum SessionStep {
    NOT_STARTED("NOT_STARTED"),
    VALUATION("VALUATION"),
    RECEPTION("RECEPTION"),
    REPAIRS_WORKSHOP("REPAIRS_WORKSHOP"),
    TECHNICAL_WORKSHOP("TECHNICAL_WORKSHOP"),
    WASH_AND_SHINE("WASH_AND_SHINE"),
    PHOTO("PHOTO"),
    PUBLISHING("PUBLISHING");

    private String step;

    SessionStep(final String step) {
        this.step = step;
    }

    public String getStep() {
        return step;
    }
}
