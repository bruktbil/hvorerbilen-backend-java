package domain;

import org.mongodb.morphia.annotations.Property;

public class SessionStates {
    private static final String NOT_STARTED_STEP_FIELD = "notStartedStep";
    private static final String VALUATION_STEP_FIELD = "valuationStep";
    private static final String RECEPTION_STEP_FIELD = "receptionStep";
    private static final String REPAIRS_WORKSHOP_STEP_FIELD = "repairsWorkshopStep";
    private static final String TECHNICAL_WORKSHOP_STEP_FIELD = "technicalWorkshopStep";
    private static final String WASH_AND_SHINE_STEP_FIELD = "washAndShineStep";
    private static final String PHOTO_STEP_FIELD = "photoStep";
    private static final String PUBLISHING_STEP_FIELD = "publishingStep";

    @Property(NOT_STARTED_STEP_FIELD)
    private SessionStepState notStarted = SessionStepState.IN_PROGRESS;
    @Property(VALUATION_STEP_FIELD)
    private SessionStepState valuation = SessionStepState.NOT_STARTED;
    @Property(RECEPTION_STEP_FIELD)
    private SessionStepState reception = SessionStepState.NOT_STARTED;
    @Property(REPAIRS_WORKSHOP_STEP_FIELD)
    private SessionStepState repairsWorkshop = SessionStepState.NOT_STARTED;
    @Property(TECHNICAL_WORKSHOP_STEP_FIELD)
    private SessionStepState technicalWorkshop = SessionStepState.NOT_STARTED;
    @Property(WASH_AND_SHINE_STEP_FIELD)
    private SessionStepState washAndShine = SessionStepState.NOT_STARTED;
    @Property(PHOTO_STEP_FIELD)
    private SessionStepState photo = SessionStepState.NOT_STARTED;
    @Property(PUBLISHING_STEP_FIELD)
    private SessionStepState publishing = SessionStepState.NOT_STARTED;

    public static String getFieldOfStep(final SessionStep step) {
        if (step.equals(SessionStep.VALUATION)) return VALUATION_STEP_FIELD;
        if (step.equals(SessionStep.RECEPTION)) return RECEPTION_STEP_FIELD;
        if (step.equals(SessionStep.REPAIRS_WORKSHOP)) return REPAIRS_WORKSHOP_STEP_FIELD;
        if (step.equals(SessionStep.TECHNICAL_WORKSHOP)) return TECHNICAL_WORKSHOP_STEP_FIELD;
        if (step.equals(SessionStep.WASH_AND_SHINE)) return WASH_AND_SHINE_STEP_FIELD;
        if (step.equals(SessionStep.PHOTO)) return PHOTO_STEP_FIELD;
        if (step.equals(SessionStep.PUBLISHING)) return PUBLISHING_STEP_FIELD;

        return NOT_STARTED_STEP_FIELD;
    }

    public SessionStepState getNotStarted() {
        return notStarted;
    }

    public SessionStepState getValuation() {
        return valuation;
    }

    public void setValuation(SessionStepState valuation) {
        this.valuation = valuation;
    }

    public SessionStepState getReception() {
        return reception;
    }

    public void setReception(SessionStepState reception) {
        this.reception = reception;
    }

    public SessionStepState getRepairsWorkshop() {
        return repairsWorkshop;
    }

    public void setRepairsWorkshop(SessionStepState repairsWorkshop) {
        this.repairsWorkshop = repairsWorkshop;
    }

    public SessionStepState getTechnicalWorkshop() {
        return technicalWorkshop;
    }

    public void setTechnicalWorkshop(SessionStepState technicalWorkshop) {
        this.technicalWorkshop = technicalWorkshop;
    }

    public SessionStepState getWashAndShine() {
        return washAndShine;
    }

    public void setWashAndShine(SessionStepState washAndShine) {
        this.washAndShine = washAndShine;
    }

    public SessionStepState getPhoto() {
        return photo;
    }

    public void setPhoto(SessionStepState photo) {
        this.photo = photo;
    }

    public SessionStepState getPublishing() {
        return publishing;
    }

    public void setPublishing(SessionStepState publishing) {
        this.publishing = publishing;
    }

    public SessionStepState getStateOfStep(final SessionStep step) {
        if (step.equals(SessionStep.VALUATION)) return valuation;
        if (step.equals(SessionStep.RECEPTION)) return reception;
        if (step.equals(SessionStep.REPAIRS_WORKSHOP)) return repairsWorkshop;
        if (step.equals(SessionStep.TECHNICAL_WORKSHOP)) return technicalWorkshop;
        if (step.equals(SessionStep.WASH_AND_SHINE)) return washAndShine;
        if (step.equals(SessionStep.PHOTO)) return photo;
        if (step.equals(SessionStep.PUBLISHING)) return publishing;

        return notStarted;
    }
}
