package client;

import com.mongodb.MongoClient;
import dao.SessionDao;
import domain.*;
import org.bson.types.ObjectId;
import org.mongodb.morphia.Key;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.query.Criteria;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;
import singleton.MorphiaConfig;

import java.util.List;

public class SessionClient {
    private final SessionDao sessionDao;

    public SessionClient() {
        final MongoClient mongoClient = MorphiaConfig.getInstance().getMongoClient();
        final Morphia morphia = MorphiaConfig.getInstance().getMorphia();

        sessionDao = new SessionDao(mongoClient, morphia, MorphiaConfig.DB_NAME);
    }

    public Object save(final Session session) {
        final Key<Session> savedSessionKey = sessionDao.getDatastore().save(session);
        return savedSessionKey.getId();
    }

    public List<Session> getAll() {
        final Query<Session> sessionQuery = sessionDao.getDatastore().createQuery(Session.class);
        return sessionDao.find(sessionQuery).asList();
    }

    public Session getById(final String id) {
        return sessionDao.getDatastore().get(Session.class, new ObjectId(id));
    }

    private Query<Session> getFindSessionByIdQuery(final String id) {
        final Query<Session> query = sessionDao.getDatastore().createQuery(Session.class);
        final Criteria entityMathcesPathId = query.criteria("id").equal(new ObjectId(id));
        query.and(entityMathcesPathId);
        return query;
    }

    public Object updateSessionFieldBySessionId(final String id, final String field, final Object updatedField) {
        final UpdateOperations<Session> updateOperation = sessionDao
                .getDatastore()
                .createUpdateOperations(Session.class)
                .set(field, updatedField);

        final Query<Session> query = getFindSessionByIdQuery(id);
        return sessionDao.getDatastore().update(query, updateOperation).getUpdatedCount();
    }

    public int addToSessionFieldById(final String id, final String field, final Object elementToAdd) {
        final boolean addDuplicateValues = true;
        final UpdateOperations<Session> updateOperation = sessionDao
                .getDatastore()
                .createUpdateOperations(Session.class)
                .add(field, elementToAdd, addDuplicateValues);

        final Query<Session> query = getFindSessionByIdQuery(id);
        return sessionDao.getDatastore().update(query, updateOperation).getUpdatedCount();
    }

    public int deleteAll() {
        final Query<Session> sessionQuery = sessionDao.getDatastore().createQuery(Session.class);
        sessionQuery.and(sessionQuery.criteria("id").exists());
        return sessionDao.deleteByQuery(sessionQuery).getN();
    }

    public int toggleActiveStepCompleted(final String id) {
        try {
            final Session session = getById(id);
            final SessionStep currentStep = session.getCurrentStep();
            final SessionStepState currentStepState = session.getStates().getStateOfStep(currentStep);
            final String activeStepField = SessionStates.getFieldOfStep(currentStep);

            final UpdateOperations<Session> updateOperation = sessionDao
                    .getDatastore()
                    .createUpdateOperations(Session.class)
                    .set("states." + activeStepField, SessionStepState.getOppositeState(currentStepState));

            return sessionDao.update(getFindSessionByIdQuery(id), updateOperation).getUpdatedCount();
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public int setNextStep(final String id, final SessionStep newStep) {
        try {
            final SessionStep currentStep = getById(id).getCurrentStep();
            final UpdateOperations<Session> updateCurrentStep = sessionDao
                    .getDatastore()
                    .createUpdateOperations(Session.class)
                    .set("states." + SessionStates.getFieldOfStep(currentStep), SessionStepState.FINISHED)
                    .set("states." + SessionStates.getFieldOfStep(newStep), SessionStepState.IN_PROGRESS)
                    .set("currentStep", newStep);

            return sessionDao.update(getFindSessionByIdQuery(id), updateCurrentStep).getUpdatedCount();
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public int updateSessionLabourOperationByIndex(final String sessionId, final String labourOperationIndex, final Labour labour) {
        try {
            final UpdateOperations<Session> updateLabourOperationWithGivenIndex = sessionDao
                    .getDatastore()
                    .createUpdateOperations(Session.class)
                    .disableValidation()
                    .set(Session.LABOUR_OPERATIONS_FIELD_NAME + "." + labourOperationIndex, labour);

            return sessionDao.update(getFindSessionByIdQuery(sessionId), updateLabourOperationWithGivenIndex).getUpdatedCount();
        } catch (final Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public int deleteSessionLabourOperationByIndex(final String sessionId, final String labourOperationIndex) {
        try {
            final Session session = getById(sessionId);
            final int indexAsInt = Integer.valueOf(labourOperationIndex);
            final List<Labour> updatedLabourOperations = session.getLabourOperations();
            updatedLabourOperations.remove(indexAsInt);

            final UpdateOperations<Session> deleteLabourOperationWithGivenIndex = sessionDao
                    .getDatastore()
                    .createUpdateOperations(Session.class)
                    .disableValidation()
                    .set(Session.LABOUR_OPERATIONS_FIELD_NAME, updatedLabourOperations);

            return sessionDao
                    .update(getFindSessionByIdQuery(sessionId), deleteLabourOperationWithGivenIndex)
                    .getUpdatedCount();
        } catch (final Exception e) {
            e.printStackTrace();
            return -1;
        }
    }
}
