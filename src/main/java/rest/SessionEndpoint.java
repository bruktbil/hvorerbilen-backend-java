package rest;

import client.SessionClient;
import com.google.gson.Gson;
import domain.*;
import spark.Spark;
import util.CorsUtils;

import java.util.Optional;
import java.util.stream.Stream;

import static domain.Session.*;

public class SessionEndpoint {
    private final String sessionIdParam = ":sessionid";
    private final String countryParam = ":country";
    private final String indexParam = ":index";
    private final String sessionPath = "/whereiscar/api/country/" + countryParam + "/session";

    private final String sessionById = sessionPath + "/" + sessionIdParam;
    private final String carInfoById = sessionById + "/carinfo";
    private final String usedCarInfoById = sessionById + "/usedcarinfo";
    private final String labourOperationsById = sessionById + "/labouroperations";
    private final String labourOperationIndexById = sessionById + "/labouroperations/" + indexParam;
    private final String commentsById = sessionById + "/comments";
    private final String logItemsById = sessionById + "/logitems";
    private final String statesById = sessionById + "/states";
    private final String finishedInProcessById = sessionById + "/finishedinprocess";
    private final String soldById = sessionById + "/sold";
    private final String acquisitionCarById = sessionById + "/acquisitioncar";

    private final Gson gson = new Gson();
    private SessionClient client = new SessionClient();

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    private static <T> Stream<T> streamOptional(final Optional<T> optional) {
        return optional.isPresent() ? Stream.of(optional.get()) : Stream.empty();
    }

    public void setupEndpoint() {
        final int port = Stream.concat(
                streamOptional(Optional.ofNullable(System.getProperty("PORT"))),
                streamOptional(Optional.ofNullable(System.getenv("PORT"))))
                .mapToInt(Integer::parseInt).findFirst().orElse(8080);

        Spark.port(port);
        CorsUtils.enableCORS("*", "*", "*");

        post();
        getAll();
        getById();
        put();
        delete();
        deleteAll();
    }

    private void post() {
        Spark.post(sessionPath, (request, response) -> {
            final Session session = gson.fromJson(request.body(), Session.class);
            return client.save(session);
        }, gson::toJson);

        setupAddToListByIdPostRoute(labourOperationsById, LABOUR_OPERATIONS_FIELD_NAME, Labour.class);
        setupAddToListByIdPostRoute(commentsById, COMMENTS_FIELD_NAME, Comment.class);
        setupAddToListByIdPostRoute(logItemsById, LOG_ITEMSS_FIELD_NAME, LogItem.class);

        Spark.post(statesById, (request, response) -> {
            final String id = request.params(sessionIdParam);
            final SessionStep newStep = gson.fromJson(request.body(), SessionStep.class);
            return client.setNextStep(id, newStep);

        });
    }

    private <T> void setupAddToListByIdPostRoute(final String path, final String fieldName, final Class<T> typeClass) {
        Spark.post(path, (request, response) -> {
            final String idParameter = request.params(sessionIdParam);
            return client.addToSessionFieldById(idParameter, fieldName, gson.fromJson(request.body(), typeClass));
        });
    }

    private void getAll() {
        Spark.get(sessionPath, (request, response) -> client.getAll(), gson::toJson);
    }

    private void getById() {
        Spark.get(sessionById, (request, response) -> {
            final String idParam = request.params(sessionIdParam);
            final Session session = client.getById(idParam);

            if (session == null) {
                final int notFoundStatus = 404;
                response.status(notFoundStatus);
            }

            return session;
        }, gson::toJson);
    }

    private void put() {
        Spark.put(carInfoById, (request, response) ->
                client.updateSessionFieldBySessionId(
                        request.params(sessionIdParam),
                        CAR_INFO_FIELD_NAME,
                        gson.fromJson(request.body(), CarInfo.class)));

        Spark.put(usedCarInfoById, (request, response) ->
                client.updateSessionFieldBySessionId(
                        request.params(sessionIdParam),
                        USED_CAR_INFO_FIELD_NAME,
                        gson.fromJson(request.body(), UsedCarInfo.class)));

        Spark.put(finishedInProcessById, (request, response) ->
                client.updateSessionFieldBySessionId(
                        request.params(sessionIdParam),
                        FINISHED_IN_PROCESS_FIELD_NAME,
                        gson.fromJson(request.body(), Boolean.class)));

        Spark.put(soldById, (request, response) ->
                client.updateSessionFieldBySessionId(
                        request.params(sessionIdParam),
                        SOLD_FIELD_NAME,
                        gson.fromJson(request.body(), Boolean.class)));

        Spark.put(acquisitionCarById, (request, response) ->
                client.updateSessionFieldBySessionId(
                        request.params(sessionIdParam),
                        ACQUISITION_CAR_FIELD_NAME,
                        gson.fromJson(request.body(), Boolean.class)));

        Spark.put(statesById, (request, response) ->
                client.toggleActiveStepCompleted(request.params(sessionIdParam)));

        Spark.put(labourOperationIndexById, (request, response) ->
                client.updateSessionLabourOperationByIndex(
                        request.params(sessionIdParam),
                        request.params(indexParam),
                        gson.fromJson(request.body(), Labour.class)));
    }

    private void delete() {
        Spark.delete(labourOperationIndexById, (request, response) ->
                client.deleteSessionLabourOperationByIndex(request.params(sessionIdParam), request.params(indexParam)));
    }

    // TODO this is just a convenience method when debugging and testing, remove in production
    private void deleteAll() {
        Spark.delete(sessionPath, (request, response) -> client.deleteAll());
    }
}
