package dao;

import com.mongodb.MongoClient;
import domain.Session;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.dao.BasicDAO;

public class SessionDao extends BasicDAO<Session, String> {
    public SessionDao(MongoClient mongoClient, Morphia morphia, String dbName) {
        super(mongoClient, morphia, dbName);
    }
}
