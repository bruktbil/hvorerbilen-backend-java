package main;

import rest.SessionEndpoint;

public class Main {
    public static void main(String[] args) {
        new SessionEndpoint().setupEndpoint();
    }
}
