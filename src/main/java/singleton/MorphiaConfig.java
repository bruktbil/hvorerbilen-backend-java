package singleton;

import com.mongodb.MongoClient;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.*;
import de.flapdoodle.embed.mongo.config.processlistener.IMongoProcessListener;
import de.flapdoodle.embed.mongo.config.processlistener.ProcessListenerBuilder;
import de.flapdoodle.embed.mongo.distribution.Version;
import de.flapdoodle.embed.process.runtime.Network;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

import java.io.File;

public class MorphiaConfig {
    public static final String DB_NAME = "hvorerbilen";
    private static final String DOMAIN_PACKAGE = "domain";
    private static MorphiaConfig morphiaConfig;
    private final Morphia morphia;
    private final MongoClient mongoClient;

    private MorphiaConfig() {
        final MongodStarter instance = MongodStarter.getDefaultInstance();
        final int mongoDbPort = 27001;
        final String databaseDir = "hvorerbilen_mongo";
        final Storage storage = new Storage(databaseDir, null, 0);

        try {
            final int syncDelay = 10;
            final IMongoCmdOptions cmdOptions = new MongoCmdOptionsBuilder()
                    .syncDelay(syncDelay)
                    .build();

            final Net netConfig = new Net(mongoDbPort, Network.localhostIsIPv6());
            final IMongoProcessListener processListener = new ProcessListenerBuilder()
                    .copyDbFilesBeforeStopInto(new File(databaseDir))
                    .build();
            final IMongodConfig mongodConfig = new MongodConfigBuilder()
                    .version(Version.Main.PRODUCTION)
                    .net(netConfig)
                    .replication(storage)
                    .processListener(processListener)
                    .cmdOptions(cmdOptions)
                    .build();

            instance.prepare(mongodConfig).start();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }

        morphia = new Morphia();
        morphia.mapPackage(DOMAIN_PACKAGE);

        mongoClient = new MongoClient("localhost", mongoDbPort);
        Datastore datastore = morphia.createDatastore(mongoClient, DB_NAME);
        datastore.ensureIndexes();
    }

    public static MorphiaConfig getInstance() {
        if (morphiaConfig == null) {
            morphiaConfig = new MorphiaConfig();
        }

        return morphiaConfig;
    }

    public Morphia getMorphia() {
        return morphia;
    }

    public MongoClient getMongoClient() {
        return mongoClient;
    }
}
