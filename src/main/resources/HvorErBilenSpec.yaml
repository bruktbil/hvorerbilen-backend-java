swagger: '2.0'
info:
  title: WhereIsTheCar
  description: Where is the car
  version: 0.0.1
host: apidev.moller.local
basePath: /WhereIsTheCarEndpoint/api
schemes:
  - http
consumes:
  - application/json
produces:
  - application/json
paths:
  '/session/':
    parameters:
        - $ref: '#/parameters/username'
        - $ref: '#/parameters/correlationId'
    post:
      operationId: createNewSession
      summary: Creates a new session
      description: Creates a new session
      parameters:
        - name: Session
          description: Session
          in: body
          required: true
          schema:
            $ref: '#/definitions/Session'
      responses:
        200:
          $ref: '#/responses/success'
        204:
          $ref: '#/responses/notFound'
        400:
          $ref: '#/responses/badRequest'

  '/session/{sessionId}':
    parameters:
        - $ref: '#/parameters/username'
        - $ref: '#/parameters/correlationId'
        - $ref: '#/parameters/sessionId'
    get:
      operationId: getSession
      summary: Gets one specific session
      description: Gets one specific session
      responses:
        200:
          description: Successfully got session
          schema:
            $ref: '#/definitions/Session'
        204:
          $ref: '#/responses/notFound'
        400:
          $ref: '#/responses/badRequest'

  '/session/{sessionId}/carInfo/':
    parameters:
      - $ref: '#/parameters/username'
      - $ref: '#/parameters/correlationId'
      - $ref: '#/parameters/sessionId'
    put:
      operationId: updateCarInfo
      summary: Updates information on the car
      description: Updates information on the car
      parameters:
        - name: carInfo
          description: Carinfo
          in: body
          required: true
          schema:
            $ref: '#/definitions/CarInfo'
      responses:
        201:
          $ref: '#/responses/updated'
        204:
          $ref: '#/responses/notFound'
    get:
      operationId: getCarInfo
      summary: Gets information about a specific car
      description: Gets information about a specific car
      responses:
        200:
          description: Successfully got carInfo
          schema:
            $ref: '#/definitions/CarInfo'
        204:
          $ref: '#/responses/notFound'

  '/session/{sessionId}/usedCarInfo/':
    parameters:
      - $ref: '#/parameters/username'
      - $ref: '#/parameters/correlationId'
      - $ref: '#/parameters/sessionId'
    put:
      operationId: updateUsedCarInfo
      summary: Updates information about a specific used car
      description: Updates information about a specific used car
      parameters:
        - name: UsedCarInfo
          description: UsedCarinfo
          in: body
          required: true
          schema:
            $ref: '#/definitions/UsedCarInfo'
      responses:
        201:
          $ref: '#/responses/updated'
        204:
          $ref: '#/responses/notFound'
    get:
      operationId: getUsedCarInfo
      summary: Gets info about the used car from a specific session
      description: Gets info about the used car from a specific session
      responses:
        200:
          description: Successfully got used car information
          schema:
            $ref: '#/definitions/UsedCarInfo'
        204:
          $ref: '#/responses/notFound'

  '/session/{sessionId}/labourOperations/':
    parameters:
      - $ref: '#/parameters/username'
      - $ref: '#/parameters/correlationId'
      - $ref: '#/parameters/sessionId'
    post:
      operationId: createLabour
      summary: Adds labour to car
      description: Adds labour to a car
      parameters:
        - name: Labour
          description: Labour
          in: body
          required: true
          schema:
            $ref: '#/definitions/Labour'
      responses:
        200:
            $ref: '#/responses/success'
        204:
            $ref: '#/responses/notFound'
    get:
      operationId: getLabourOperations
      summary: Gets labouroperations for a specific session
      description: Gets labouroperations for a specific session
      responses:
        200:
          description: Successfully got labouroperations
          schema:
            $ref: '#/definitions/LabourOperations'
        204:
            $ref: '#/responses/notFound'

  '/session/{sessionId}/labourOperations/{labourId}':
    parameters:
      - $ref: '#/parameters/username'
      - $ref: '#/parameters/correlationId'
      - $ref: '#/parameters/labourId'
      - $ref: '#/parameters/sessionId'
    put:
      operationId: updateLabour
      summary: Updates the labour on a specific labour.
      description: Updates the labour on a specific labour.
      parameters:
        - name: labour
          description: labour
          in: body
          required: true
          schema:
            $ref: '#/definitions/Labour'
      responses:
        201:
            $ref: '#/responses/updated'
        204:
            $ref: '#/responses/notFound'
    delete:
      operationId: deleteLabour
      summary: Deletes a specific labour
      description: Deletes a specific labour
      responses:
        200:
            $ref: '#/responses/deleted'
        204:
            $ref: '#/responses/notFound'

  '/session/{sessionId}/log/':
    parameters:
      - $ref: '#/parameters/username'
      - $ref: '#/parameters/correlationId'
      - $ref: '#/parameters/sessionId'
    get:
      operationId: getSessionLog
      summary: Gets the log for a specific session
      description: Gets the log for a specific session
      responses:
        200:
          description: Successfully got the log
          schema:
            $ref: '#/definitions/SessionLog'
        204:
          $ref: '#/responses/notFound'

  '/session/{sessionId}/comments/':
    parameters:
      - $ref: '#/parameters/username'
      - $ref: '#/parameters/correlationId'
      - $ref: '#/parameters/sessionId'
    post:
      operationId: createComment
      summary: Creates comment to a specific session
      description: Creates comment to a specific session
      parameters:
        - name: Comment
          description: Comment about the session
          in: body
          required: true
          schema:
            $ref: '#/definitions/Comment'
      responses:
        200:
          $ref: '#/responses/created'
        204:
          $ref: '#/responses/notFound'
    get:
      operationId: getSessionComments
      summary: Gets comments about a specific session
      description: Gets comments about a specific session
      responses:
        200:
          description: Successfully got comments
          schema:
            $ref: '#/definitions/Comments'
        204:
          $ref: '#/responses/notFound'

  '/session/{sessionId}/sessionState/':
    parameters:
      - $ref: '#/parameters/username'
      - $ref: '#/parameters/correlationId'
      - $ref: '#/parameters/sessionId'
    post:
      operationId: createSessionStepState
      summary: Creates session step state for a specific session
      description: Creates session step state for a specific session
      parameters:
        - name: SessionState
          description: SessionState
          in: body
          required: true
          schema:
            $ref: '#/definitions/SessionStepState'
      responses:
        200:
          $ref: '#/responses/created'
        204:
          $ref: '#/responses/notFound'
    get:
      operationId: getSessionState
      summary: Gets the session state about a specific session
      description: Gets the session state about a specific session
      responses:
        200:
          description: Successfully got session state
          schema:
            $ref: '#/definitions/SessionStates'
        204:
          $ref: '#/responses/notFound'

definitions:
  Session:
    description: A session
    properties:
      sessionId:
        $ref: '#/definitions/SessionId'
      carInfo:
        $ref: '#/definitions/CarInfo'
      usedCarInfo:
        $ref: '#/definitions/UsedCarInfo'
      labourOperations:
        $ref: '#/definitions/LabourOperations'
      comments:
        $ref: '#/definitions/Comments'
      sessionLog:
        $ref: '#/definitions/SessionLog'
      sessionStates:
        $ref: '#/definitions/SessionStates'
  CarInfo:
    description: Information about the car
    properties:
      regNr:
        $ref: '#/definitions/RegNr'
      brand:
        $ref: '#/definitions/Brand'
      model:
        $ref: '#/definitions/Model'
      color:
        $ref: '#/definitions/Color'
      mileage:
        $ref: '#/definitions/Mileage'
      modelYear:
        $ref: '#/definitions/ModelYear'
  UsedCarInfo:
    description: Information about the used car
    properties:
      usedCarNumber:
        $ref: '#/definitions/UsedCarNumber'
      location:
        $ref: '#/definitions/Location'
      bought:
        $ref: '#/definitions/Bought'
      owner:
        $ref: '#/definitions/Owner'
      assessedByUser:
        $ref: '#/definitions/AssessedByUser'
      departmentMtakst:
        $ref: '#/definitions/DepartmentCarValuation'
  LabourOperations:
    description: Information about the labour on a car
    properties:
      labours:
        type: array
        items:
          $ref: '#/definitions/Labour'
  Labour:
    description: Information about a labour
    properties:
      labourId:
        $ref: '#/definitions/LabourId'
      labourDescription:
        $ref: '#/definitions/LabourDescription'
      labourLocation:
        $ref: '#/definitions/LabourLocation'
      labourFromDate:
        $ref: '#/definitions/LabourFromDate'
      labourFromTime:
        $ref: '#/definitions/LabourFromTime'
      labourToDate:
        $ref: '#/definitions/LabourToDate'
      labourToTime:
        $ref: '#/definitions/LabourToTime'
      labourFinished:
        $ref: '#/definitions/LabourFinished'
  Comment:
    description: Comment that is posted about the car
    properties:
      commentId:
        $ref: '#/definitions/CommentId'
      sessionState:
        $ref: '#/definitions/CommentSessionState'
      commentText:
        $ref: '#/definitions/CommentText'

  SessionLog:
    description: Log
    properties:
      log:
        type: array
        items:
          $ref: '#/definitions/LogItem'
  SessionId:
    description: The id of a session
    properties:
      sessionId:
        type: integer
  RegNr:
    description: The registration number of a car
    properties:
      regNr:
        type: integer
  Brand:
    description: The brand of a car
    properties:
      brand:
        type: string
  Model:
    description: Car model of a car
    properties:
      model:
        type: string
  Color:
    description: The color of a car
    properties:
      color:
        type: string
  Mileage:
    description: The milage of a car
    properties:
      milage:
        type: integer
  ModelYear:
    description: The modelyear of a car
    properties:
      regNr:
        type: integer
  UsedCarNumber:
    description: The used car number of a car
    properties:
      usedCarNumber:
        type: integer
  Location:
    description: The location of a used car
    properties:
      location:
        type: integer
  Bought:
    description: The date of purchase of a used car
    properties:
      bought:
        type: string
        format: date
  Owner:
    description: The owner of a used car
    properties:
      owner:
        type: string
  AssessedByUser:
    description: Assessed by username
    properties:
      assessedByUser:
        type: string
  DepartmentCarValuation:
    description: The department in carvaluation of a used car
    properties:
      departmentCarValuation:
        type: string
  CommentId:
    description: The id of a comment
    properties:
      commentId:
        type: integer
  CommentText:
    description: The comment text
    properties:
      commentText:
        type: string
  LabourId:
    description: The id of a labour
    properties:
      labourId:
        type: integer
  LabourLocation:
    description: Location of a labour
    properties:
      work:
        type: string
  LabourDescription:
    description: Description of a work
    properties:
      workDescription:
        type: string
  LabourFromDate:
    description: The start date of a work
    properties:
      fromDate:
        type: string
        format: date
  LabourFromTime:
    description: The start time of a work
    properties:
      fromDate:
        type: string
        format: date
  LabourToDate:
    description: The end date of a work
    properties:
      fromDate:
        type: string
        format: date
  LabourToTime:
    description: The end time of a work
    properties:
      fromDate:
        type: string
        format: date
  LabourFinished:
    description: Is the work finished or not
    properties:
      workFinished:
        type: boolean
  LogItem:
    description: Describes a logitem
    properties:
      actionType:
        $ref: '#/definitions/ActionType'
      logValue:
        $ref: '#/definitions/LogValue'
      logDate:
        $ref: '#/definitions/LogDate'
      userPerformingAction:
        $ref: '#/definitions/UserPerformingAction'
  LogDate:
    description: logDate
    properties:
      logDate:
        type: string
  UserPerformingAction:
    description: logtype
    properties:
      userPerformingAction:
        type: string
  
  SessionStepState:
    description: Describes the session step
    type: string
    enum:
      - NOT_STARTED
      - IN_PROGRESS
      - FINISHED
  Comments:
    description: Array of comment about the session
    type: array
    items:
      $ref: '#/definitions/Comment'
  
  CommentSessionState:
    description: Describes the state when the comment was made
    type: string
    enum:
      - VALUATION
      - RECEPTION
      - REPAIRS_WORKSHOP
      - TECHNICAL_WORKSHOP
      - WASH_AND_SHINE
      - PHOTO
      - PUBLISHING
  
  SessionStates:
    description: Holds the state for the session
    properties:
      valuationState:
        $ref: '#/definitions/SessionStepState'
      receptionState: 
        $ref: '#/definitions/SessionStepState'
      repairsWorkshopState: 
        $ref: '#/definitions/SessionStepState'
      technicalWorkshopState: 
        $ref: '#/definitions/SessionStepState'
      washAndShineState: 
        $ref: '#/definitions/SessionStepState'
      photoState: 
        $ref: '#/definitions/SessionStepState'
      publishingState: 
        $ref: '#/definitions/SessionStepState'
        
  ActionType:
    description: Describes the action performed by the user.
    properties:
      actionType:
        type: string
  LogValue:
    description: The change that occured with the action
    properties:
      logValue:
        type: string
        
  ErrorModel:
    properties:
      httpCode:
        type: integer
        description: The HTTP status code
      httpDescription:
        type: string
        description: The description of the HTTP status
      httpMethod:
        type: string
        description: The HTTP verb
      uri:
        type: string
        description: The current URI that triggered the error
      code:
        type: string
        description: The error code
      developerMessage:
        type: string
        format: uuid
        description: The correlation ID of this transaction
      errors:
        type: array
        items:
          $ref: '#/definitions/ErrorItem'
        description: The errors that caused the erroneous response
  ErrorItem:
    properties:
      field:
        description: The field value that is a target for the error
        type: string
      message:
        type: string 
        
responses:
  success:
    description: Success
    schema:
      type: string
      description: message
  notFound:
    description: Not found
  badRequest:
    description: Bad request
  conflict:
    description: The server failed to process the request because the request entity conflicts with the state
    schema:
      $ref: '#/definitions/ErrorModel'
  updated:
    description: The resource was successfully updated
  deleted:
    description: The resource was successfully deleted
  created:
    description: The resource was successfully created

parameters:
  sessionId:
    name: sessionId
    in: path
    description: sessionId
    required: true
    type: integer
    format: int32
  username:
    name: username
    in: header
    description: Username of the originating caller
    required: true
    type: string
  correlationId:
    name: correlationId
    in: header
    description: Unique request id from the originating caller
    required: true
    type: string
  labourId:
    name: labourId
    in: path
    description: The id of the labour
    required: true
    type: integer
    format: int32

  