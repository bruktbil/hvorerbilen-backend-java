package domain;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SessionStepStateTest {
    @Test
    public void getOppositeState() throws Exception {
        final SessionStepState oppositeOfInProgress = SessionStepState.getOppositeState(SessionStepState.IN_PROGRESS);
        assertEquals(SessionStepState.FINISHED, oppositeOfInProgress);

        final SessionStepState oppositeOfFinished = SessionStepState.getOppositeState(SessionStepState.FINISHED);
        assertEquals(SessionStepState.IN_PROGRESS, oppositeOfFinished);

        final SessionStepState oppositeOfNotStarted = SessionStepState.getOppositeState(SessionStepState.NOT_STARTED);
        assertEquals(SessionStepState.IN_PROGRESS, oppositeOfNotStarted);
    }

}