package client;

import domain.Comment;
import domain.Session;
import domain.SessionStep;
import domain.SessionStepState;
import org.bson.types.ObjectId;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;
import java.util.List;

import static domain.Session.COMMENTS_FIELD_NAME;
import static domain.Session.CURRENT_STEP_FIELD_NAME;
import static org.junit.Assert.*;

public class SessionClientIT {
    private SessionClient client = new SessionClient();
    private Session savedSession = new Session();
    private String savedSessionId;

    @Before
    public void setUp() throws Exception {
        savedSessionId = client.save(savedSession).toString();
        savedSession.setId(savedSessionId);
    }

    @After
    public void tearDown() throws Exception {
        client.deleteAll();
    }

    @Test
    public void save() throws Exception {
        final int initialSize = client.getAll().size();
        final Session newSession = new Session();
        client.save(newSession);

        assertNotEquals(initialSize, client.getAll().size());

        final Session savedSession = client.getById(savedSessionId);
        assertNotNull(savedSession);
        assertNotNull(savedSession.getStates());
    }

    @Test
    public void getAll() throws Exception {
        assertEquals(1, client.getAll().size());
    }

    @Test
    public void getById() throws Exception {
        final Session session = client.getById(savedSessionId);
        assertNotNull(session);
        assertNotNull(session.getCurrentStep());
        assertNotNull(session.getStates());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getByInvalidIdThrowsException() throws Exception {
        client.getById("INVALID_SESSION_ID");
    }

    @Test
    public void getByNonExistingIdReturnsNull() throws Exception {
        final String idNotReferringToAnyEntity = new ObjectId(new Date()).toHexString();
        final Session session = client.getById(idNotReferringToAnyEntity);
        assertNull(session);
    }

    @Test
    public void updateSessionFieldBySessionId() throws Exception {
        final SessionStep oldStep = savedSession.getCurrentStep();
        client.updateSessionFieldBySessionId(savedSessionId, CURRENT_STEP_FIELD_NAME, SessionStep.PUBLISHING);

        final Session updatedSession = client.getById(savedSessionId);
        assertNotEquals(updatedSession.getCurrentStep(), oldStep);
    }

    @Test
    public void addToSessionFieldById() throws Exception {
        final Session savedSession = client.getById(savedSessionId);
        assertEquals(0, savedSession.getComments().size());


        final Comment comment = new Comment();
        final String commentText = "TEST_TEXT";
        final SessionStep testStep = SessionStep.RECEPTION;
        comment.setText(commentText);
        comment.setCategory(testStep);

        final int updatedCount = client.addToSessionFieldById(savedSessionId, COMMENTS_FIELD_NAME, comment);
        assertEquals(1, updatedCount);

        final Session updatedSession = client.getById(savedSessionId);
        final List<Comment> updatedComments = updatedSession.getComments();
        final int updatedCommentsSize = updatedComments.size();
        assertEquals(1, updatedCommentsSize);

        final Comment object = updatedComments.get(updatedCommentsSize - 1);
        assertNotNull(object);
        assertTrue(comment.equals(object));
    }

    @Test
    public void deleteAll() throws Exception {
        assertEquals(1, client.deleteAll());
        assertEquals(0, client.getAll().size());
    }

    @Test
    public void toggleActiveStepCompleted() throws Exception {
        final Session savedSession = client.getById(savedSessionId);
        assertTrue(savedSession.getCurrentStep().equals(SessionStep.NOT_STARTED));
        final SessionStepState stateOfCurrentStep = savedSession.getStates().getStateOfStep(SessionStep.NOT_STARTED);
        assertEquals(SessionStepState.IN_PROGRESS, stateOfCurrentStep);

        assertEquals(1, client.toggleActiveStepCompleted(savedSessionId));
        final Session updatedSession = client.getById(savedSessionId);
        final SessionStepState updatedStateOfCurrentStep = updatedSession.getStates().getStateOfStep(SessionStep.NOT_STARTED);
        assertEquals(SessionStepState.FINISHED, updatedStateOfCurrentStep);

        assertEquals(1, client.toggleActiveStepCompleted(savedSessionId));
        final Session resetSession = client.getById(savedSessionId);
        final SessionStepState resetStateOfCurrentStep = resetSession.getStates().getStateOfStep(SessionStep.NOT_STARTED);
        assertEquals(SessionStepState.IN_PROGRESS, resetStateOfCurrentStep);

        assertEquals(1, client.toggleActiveStepCompleted(savedSessionId));
        final Session savedSession2 = client.getById(savedSessionId);
        final SessionStepState updatedStateOfCurrentStep2 = savedSession2.getStates().getStateOfStep(SessionStep.NOT_STARTED);
        assertEquals(SessionStepState.FINISHED, updatedStateOfCurrentStep2);

    }

    @Test
    public void setNextStep() throws Exception {
        assertEquals(1, client.setNextStep(savedSessionId, SessionStep.PHOTO));

        final Session fromValuationToPhotoStepSession = client.getById(savedSessionId);
        assertEquals(SessionStep.PHOTO, fromValuationToPhotoStepSession.getCurrentStep());
        assertEquals(SessionStepState.FINISHED, fromValuationToPhotoStepSession.getStates().getNotStarted());
        assertEquals(SessionStepState.IN_PROGRESS, fromValuationToPhotoStepSession.getStates().getPhoto());

        assertEquals(1, client.setNextStep(savedSessionId, SessionStep.PUBLISHING));
        final Session fromPhotoToPublishingStepSession = client.getById(savedSessionId);
        assertEquals(SessionStep.PUBLISHING, fromPhotoToPublishingStepSession.getCurrentStep());
        assertEquals(SessionStepState.FINISHED, fromPhotoToPublishingStepSession.getStates().getNotStarted());
        assertEquals(SessionStepState.FINISHED, fromPhotoToPublishingStepSession.getStates().getPhoto());
        assertEquals(SessionStepState.IN_PROGRESS, fromPhotoToPublishingStepSession.getStates().getPublishing());

        assertEquals(1, client.setNextStep(savedSessionId, SessionStep.TECHNICAL_WORKSHOP));
        final Session fromPublishingToTechnicalWorkshopStepSession = client.getById(savedSessionId);
        assertEquals(SessionStep.TECHNICAL_WORKSHOP, fromPublishingToTechnicalWorkshopStepSession.getCurrentStep());
        assertEquals(SessionStepState.FINISHED, fromPublishingToTechnicalWorkshopStepSession.getStates().getNotStarted());
        assertEquals(SessionStepState.FINISHED, fromPublishingToTechnicalWorkshopStepSession.getStates().getPhoto());
        assertEquals(SessionStepState.FINISHED, fromPublishingToTechnicalWorkshopStepSession.getStates().getPublishing());
        assertEquals(SessionStepState.IN_PROGRESS, fromPublishingToTechnicalWorkshopStepSession.getStates().getTechnicalWorkshop());
    }

}